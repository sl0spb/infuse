<?php

declare(strict_types=1);

namespace App\Service;

use PDO;
use PDOException;

class DbConnection
{
	private PDO $connection;

	public function __construct(string $host, string $dbname, string $username, string $password)
	{
		try {
			$this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}
	}

	public function getConnection(): PDO
	{
		return $this->connection;
	}
}