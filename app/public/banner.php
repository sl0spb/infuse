<?php

declare(strict_types=1);

require_once '../vendor/autoload.php';

use App\Service\DbConnection;
use DI\ContainerBuilder;

function validatePageUrl($page_url): string
{
	$allowedUrls = [
		'http://localhost/index1.html',
		'http://localhost/index2.html',
	];

	return in_array($page_url, $allowedUrls, true) ? $page_url : 'other';
}

session_start();
$config = require '../config/config.php';

$containerBuilder = new ContainerBuilder();
$containerBuilder->useAutowiring(true);

$containerBuilder->addDefinitions([
	DbConnection::class => DI\autowire(DbConnection::class)
		->constructor(
			$config['db']['host'],
			$config['db']['dbname'],
			$config['db']['username'],
			$config['db']['password']
		)
]);

$container    = $containerBuilder->build();
$dbConnection = $container->get(DbConnection::class);
$connection   = $dbConnection->getConnection();

$ip_address        = $_SERVER['REMOTE_ADDR'];
$user_agent        = htmlspecialchars($_SERVER['HTTP_USER_AGENT']);
$page_url          = isset($_SERVER['HTTP_REFERER']) ? validatePageUrl($_SERVER['HTTP_REFERER']) : 'other';
$current_timestamp = microtime(true);

if (!isset($_SESSION['last_execution_timestamp']) || $current_timestamp - $_SESSION['last_execution_timestamp'] >= 1) {
	$stmt = $connection->prepare("SELECT id,views_count FROM counts WHERE ip_address = INET6_ATON(?) AND user_agent = ? AND page_url = ? LIMIT 1");
	$stmt->execute([$ip_address, $user_agent, $page_url]);

	$result = $stmt->fetch(PDO::FETCH_ASSOC);

	if ($result) {
		$updated_views_count = $result['views_count'] + 1;

		$updateStmt = $connection->prepare("UPDATE counts SET views_count = ?, view_date = NOW() WHERE id = ?");
		$updateStmt->execute([$updated_views_count, $result['id']]);
	}
	else {
		$stmt = $connection->prepare("INSERT INTO counts (ip_address, user_agent, views_count, view_date, page_url) VALUES (INET6_ATON(?), ?, ?, NOW(), ?)");
		$stmt->execute([$ip_address, $user_agent, 1, $page_url]);
	}

	$_SESSION['last_execution_timestamp'] = $current_timestamp;
}

ob_start();
$image_path = $page_url === 'http://localhost/index1.html' ? './images/page1.jpg' : './images/page2.png';
header('Content-Type: image/jpeg');
readfile($image_path);
ob_end_flush();
