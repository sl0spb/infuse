CREATE TABLE IF NOT EXISTS counts
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    ip_address  VARBINARY(16)           NOT NULL,
    user_agent  VARCHAR(255)            NOT NULL,
    view_date   DATETIME                NOT NULL,
    page_url    ENUM ('http://localhost/index1.html', 'http://localhost/index2.html', 'other') NOT NULL,
    views_count INT                     NOT NULL
);

CREATE INDEX idx_counts_ip ON counts (ip_address);
CREATE UNIQUE INDEX idx_counts_ip_user_page ON counts (ip_address, user_agent, page_url);