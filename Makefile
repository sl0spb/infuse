up: docker-up-app
down: docker-down
restart: docker-down docker-up-app
init-app:  docker-down-clear docker-pull docker-build-app docker-up-app

docker-up-app:
	docker compose up -d

docker-down:
	 docker compose down --remove-orphans

docker-down-clear:
	 docker compose down -v --remove-orphans

docker-pull:
	docker compose pull

docker-build-app:
	 docker compose build --build-arg target=app_dev
