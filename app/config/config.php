<?php

declare(strict_types=1);

return [
	'db' => [
		'host'     => 'database',
		'dbname'   => 'db',
		'username' => 'root',
		'password' => 'root',
	],
];
